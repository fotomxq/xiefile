package main

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	CoreFile "xieshuqi/file"
)

//目标文件
var (
	srcFile = "./out.xlsx"
)

func main(){
	var err error
	//构建excel
	excelFile := excelize.NewFile()
	excelIndex := excelFile.NewSheet("Sheet1")
	excelFile.SetActiveSheet(excelIndex)
	//构建第一列
	excelFile.SetCellValue("Sheet1", "A1", "序号")
	excelFile.SetCellValue("Sheet1", "B1", "第1级目录")
	excelFile.SetCellValue("Sheet1", "C1", "第2级目录")
	excelFile.SetCellValue("Sheet1", "D1", "第3级目录")
	excelFile.SetCellValue("Sheet1", "E1", "文件名")
	excelFile.SetCellValue("Sheet1", "F1", "备注")
	//遍历获取当前文件夹数据包
	//获取第1级
	bFiles, err := CoreFile.GetFileList("./", nil, true)
	if err != nil{
		fmt.Println("no files, ", err)
		return
	}
	index := 2
	for _, b1File := range bFiles{
		b1FileNames, err := CoreFile.GetFileNames(b1File)
		if err != nil{
			fmt.Println("get file names, ", err)
			return
		}
		b := CoreFile.IsFolder(b1File)
		if b{
			excelFile.SetCellValue("Sheet1", fmt.Sprint("B", index), b1FileNames["name"])
			//处理第2级
			cFiles, err := CoreFile.GetFileList(b1File, nil, true)
			if err != nil{
				fmt.Println("no files, ", err)
				return
			}
			for _, c1File := range cFiles{
				c1FileNames, err := CoreFile.GetFileNames(c1File)
				if err != nil{
					fmt.Println("get file names, ", err)
					return
				}
				b := CoreFile.IsFolder(c1File)
				if b{
					excelFile.SetCellValue("Sheet1", fmt.Sprint("C", index), c1FileNames["only-name"])
					//处理第3级
					dFiles, err := CoreFile.GetFileList(c1File, nil, true)
					if err != nil{
						continue
					}
					for _, d1File := range dFiles{
						d1FileNames, err := CoreFile.GetFileNames(d1File)
						if err != nil{
							continue
						}
						b := CoreFile.IsFolder(d1File)
						if b{
							excelFile.SetCellValue("Sheet1", fmt.Sprint("D", index), d1FileNames["only-name"])
							//处理第4级
							eFiles, err := CoreFile.GetFileList(d1File, nil, true)
							if err != nil{
								continue
							}
							for _, e1File := range eFiles{
								e1FileNames, err := CoreFile.GetFileNames(e1File)
								if err != nil{
									fmt.Println("get file names, ", err)
									return
								}
								b := CoreFile.IsFolder(e1File)
								if b{
									allFiles := getFileNameAll(e1File, nil)
									for _, vAllFile := range allFiles{
										vAllFileNames, err := CoreFile.GetFileNames(vAllFile)
										if err != nil{
											continue
										}
										excelFile.SetCellValue("Sheet1", fmt.Sprint("E", index), vAllFileNames["only-name"])
										fmt.Println("find new file, ", fmt.Sprint("E", index), ", ", vAllFileNames["name"])
										index += 1
									}
								}else{
									excelFile.SetCellValue("Sheet1", fmt.Sprint("E", index), e1FileNames["only-name"])
									fmt.Println("find new file, ", fmt.Sprint("E", index), ", ", e1FileNames["name"])
								}
								index += 1
							}
						}else{
							excelFile.SetCellValue("Sheet1", fmt.Sprint("E", index), d1FileNames["only-name"])
							fmt.Println("find new file, ", fmt.Sprint("E", index), ", ", d1FileNames["name"])
						}
						index += 1
					}
				}else{
					excelFile.SetCellValue("Sheet1", fmt.Sprint("E", index), c1FileNames["only-name"])
					fmt.Println("find new file, ", fmt.Sprint("E", index), ", ", c1FileNames["name"])
				}
				index += 1
			}
		}else{
			excelFile.SetCellValue("Sheet1", fmt.Sprint("E", index), b1FileNames["only-name"])
			fmt.Println("find new file, ", fmt.Sprint("E", index), ", ", b1FileNames["name"])
		}
		index += 1
	}
	//保存文件
	err = excelFile.SaveAs(srcFile)
	if err != nil{
		fmt.Println("save file failed, ", err)
		return
	}
	fmt.Println("save file success")
}

func setFileNameToExcel(excelObj *excelize.File, key string, folderSrc string) (newFolderList []string){
	files, err := CoreFile.GetFileList(folderSrc, nil, true)
	if err != nil{
		fmt.Println("no files, ", err)
		return
	}
	index := 2
	for _, vFile := range files{
		vFileNames, err := CoreFile.GetFileNames(vFile)
		if err != nil{
			fmt.Println("get file names, ", err)
			return
		}
		b := CoreFile.IsFolder(vFile)
		if b{
			newFolderList = append(newFolderList, vFile)
		}else{
			excelObj.SetCellValue("Sheet1", fmt.Sprint(key, index), vFileNames["name"])
			index += 1
		}
	}
	return
}

//无限递归获取文件名称
func getFileNameAll(findFolder string, lastList []string) (files []string) {
	findFiles, err := CoreFile.GetFileList(findFolder, nil, true)
	if err != nil {
		return
	}
	files = lastList
	for _, v := range findFiles{
		b := CoreFile.IsFolder(v)
		if b {
			files = getFileNameAll(v, files)
		}else{
			files = append(files, v)
		}
	}
	return
}